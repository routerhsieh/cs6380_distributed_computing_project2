package proj2;

public interface ActionRegistry {
	public void register(Action action);
}
