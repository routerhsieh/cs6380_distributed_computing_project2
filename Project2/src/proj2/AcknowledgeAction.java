package proj2;

public class AcknowledgeAction implements Action {
	private ServerContext ctx;

	AcknowledgeAction(ServerContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void does(ParcelInfo info, Message message) {
		Conversation conversation = ctx.getConversation(message.getConversationId());
		conversation.acknowledge(info, message);
		if (conversation.isAllAcknowledged()) {
			AckCallback doAfterAllCallback = conversation.getCallback();
			doAfterAllCallback.doAfterAllAcknowledged();
			ctx.endConversation(conversation.getUuid());
		}
	}

	@Override
	public String[] getTypeStrings() {
		String[] typeStrings = { ParcelType.BACK.toString(),
				ParcelType.JACK.toString(), ParcelType.NACK.toString(),
				ParcelType.DACK.toString(), ParcelType.DJACK.toString() };
		return typeStrings;
	}

}
