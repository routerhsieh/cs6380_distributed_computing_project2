package proj2;

import java.util.*;

public class JoinAction implements Action {
	private ServerContext ctx;

	JoinAction(ServerContext ctx) {
		this.ctx = ctx;
	}
	
	private void printTreeNeighbors(ServerContext ctx) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Tree Neighbors: \n");
		
		Set<Integer> logicalNeighbors = ctx.getLogicalNeighbors();
		for (Integer neighborNodeId : logicalNeighbors) {
			String neighborHost = ctx.getNodeHost(neighborNodeId);
			int neighborPort = ctx.getNodePort(neighborNodeId);
			
			String neighborString = String.format("Node %d - %s:%d\n", neighborNodeId, neighborHost, neighborPort);
			stringBuilder.append(neighborString);
		}
		
		NodeServer.messageOutput(ctx, stringBuilder.toString());
	}
	
	private void sendACK(ParcelInfo info, Message message, Communicator communicator, ParcelType type) {
		int currentNodeNum = info.getDstNodeNum();
		String currentHost = ctx.getCurrentHost();
		int currentPort = ctx.getCurrentPort();
		
		int upperStreamNodeNum = info.getFromNodeNum();
		String upperStreamHost = ctx.getNodeHost(upperStreamNodeNum);
		int upperStreamPort = ctx.getNodePort(upperStreamNodeNum);
		
		
		ParcelInfo ackInfo = new ParcelInfo(currentNodeNum, upperStreamNodeNum);
		ackInfo.setDstHost(upperStreamHost);
		ackInfo.setDstPort(upperStreamPort);
		String ackText = type.toString() + ": Reply JOIN from" + currentHost + ":" + currentPort;
		Message ackMessage = new Message(currentNodeNum, ackText, type);
		ackMessage.setConversationId(message.getConversationId());
		Parcel parcel = new Parcel(type, ackInfo, ackMessage);
		communicator.send(parcel);
		ctx.endConversation(message.getConversationId());
	}
	
	private void sendJOIN(Set<Integer> downStreams,Communicator communicator) {
		Conversation conversation = ctx.beginConversation();
		conversation.setUpperStream(ctx.getCurrentNodeNum());
		conversation.setDownStreams(downStreams);
		int currentNodeNum = ctx.getCurrentNodeNum();
		NodeInfo currentNode = ctx.getCurrentNodeInfo();
		String currentHost = currentNode.getHost();
		int currentPort = currentNode.getPort();
		
		
		for (Integer downStreamNodeId : downStreams) {
			String dstHost = ctx.getNodeHost(downStreamNodeId);
			int dstPort = ctx.getNodePort(downStreamNodeId);

			ParcelInfo joinInfo = new ParcelInfo(currentNodeNum, downStreamNodeId);
			joinInfo.setDstHost(dstHost);
			joinInfo.setDstPort(dstPort);
			String joinText = "JOIN: Send from " + currentHost + ":"
					+ currentPort;
			Message joinMessage = new Message(ctx.getCurrentNodeNum(),
					joinText, ParcelType.JOIN);
			joinMessage.setConversationId(conversation.getUuid());
			Parcel joinParcel = new Parcel(ParcelType.JOIN,
					joinInfo, joinMessage);
			conversation.setCallback(new AckCallback.Default(){

				@Override
				public void doAfterAllAcknowledged() {
					doAfterAllJoinDownStreamAcknowledged();
				}

				@Override
				public void doAfterAcknowledged(ParcelInfo info, Message message) {
					doAfterJoinDownStreamAcknowledged(info, message);
				}
				
			});
			communicator.send(joinParcel);
		}
	}
	
	private void doAfterJoinDownStreamAcknowledged(final ParcelInfo info, final Message message) {
		/* This is a callback, will be called when join message sender receive ACK message from downStream */
		ParcelType type = message.getOriginalType();
		if (type == ParcelType.JACK) {
			ctx.setLogicalNeighbor(info.getFromNodeNum());
		}
		//If we receive a NACK, then do nothing unless conversation.isAllAcknowledged() is true,
		//and we leave this to the doAfterAllJoinDownStreamAcknowledged()
	}
	private void doAfterAllJoinDownStreamAcknowledged() {
		NodeInfo currNode = ctx.getCurrentNodeInfo();
		String currHost = currNode.getHost();
		int currPort = currNode.getPort();
		
		String doneMessage = String.format("Tree Building Process is done at Node %d(%s:%d)\n", currNode.getNodeNum(), currHost, currPort);
		NodeServer.messageOutput(ctx, doneMessage);
		printTreeNeighbors(ctx);
	}

	@Override
	public void does(ParcelInfo info, Message message) {
		/*
		 * 1. Get the ParcelHandler and direct neighbors from the server context(ctx) 
		 * 2. Get communicator through ParcelHandler for send() 
		 * 3. If this is the first join message we received, sent back a ACK message to source node, 
		 *    and add source node to our tree neighbor 
		 * 4. Find those direct neighbors are not in tree and re-send the join message to them 
		 * 5. Otherwise, we just sent NACK message back to source node
		 */
		Set<Integer> physicalNeighbors = ctx.getPhysicalNeighbors();
		Set<Integer> logicalNeighbors = ctx.getLogicalNeighbors();
		ParcelHandler handler = ctx.getHandler();
		Communicator communicator = handler.getCommunicator();
		
		Conversation conversation = ctx.getConversation(message.getConversationId());
		if (conversation == null) { // That means we are not initiator, because initiator's conversation will be created at CmdAction's does
			conversation = ctx.newConversation(message.getConversationId());
		}
		int upperStreamNodeNum = info.getFromNodeNum();
		boolean notInitiator = !conversation.isInitiator();

		if (ctx.isInTree()) {
			/* Send NACK back to upper stream */
			sendACK(info, message, communicator, ParcelType.NACK);
		} else {
			/*
			 * Send JACK back to upeer stream, mark upper stream as a tree
			 * neighbor and set this node is in tree now
			 */
			ctx.setInTree(true);
			if (notInitiator) {
				/*
				 * If the src/dst node number is equal, that means this node is
				 * the source node of JOIN message, so it can't add itself to
				 * its tree neighbors.
				 */
				logicalNeighbors.add(upperStreamNodeNum);
				sendACK(info, message, communicator, ParcelType.JACK);
			}

			/* Find out the direct neighbors but not our tree neighbor */
			Set<Integer> nonTreeNeighbors = new HashSet<Integer>(
					physicalNeighbors);
			if (notInitiator) {
				/*
				 * If it is source, then logical neighbors will equal to
				 * physical neighbors
				 */
				nonTreeNeighbors.removeAll(logicalNeighbors);
			}

			if (nonTreeNeighbors.size() == 0) {
				/*
				 * That means this node is leaf, so we don't need to re-send the
				 * join message anymore. Also, the "building process" is done in
				 * this node.
				 */
				int srcNodeNum = ctx.getCurrentNodeNum();
				String srcHost = ctx.getCurrentHost();
				int srcPort = ctx.getCurrentPort();
				String doneMessage = String.format(
						"Tree Building Process done at Node %d(%s:%d)\n",
						srcNodeNum, srcHost, srcPort);
				NodeServer.messageOutput(ctx, doneMessage);
				printTreeNeighbors(ctx);
			} else {
				sendJOIN(nonTreeNeighbors, communicator);
			}
		}
	}

	@Override
	public String[] getTypeStrings() {
		String[] typeStrings = {ParcelType.JOIN.toString()};
		return typeStrings;
	}

}
