package proj2;

import java.io.Serializable;

/**
 * POJO
 * @author router
 *
 */

public class Parcel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ParcelType type;
	private ParcelInfo info;
	private Message message;
	
	Parcel (ParcelType type, ParcelInfo info, Message message) {
		this.type = type;
		this.info = info;
		this.message = message;
	}
	
	public ParcelType getType() {
		return type;
	}
	public void setType(ParcelType type) {
		this.type = type;
	}
	public ParcelInfo getInfo() {
		return info;
	}
	public void setInfo(ParcelInfo info) {
		this.info = info;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
}
