package proj2;

import java.io.Serializable;
import java.util.UUID;

/**
 * 1. originalHost/originalPort only used for ACK to Broadcast
 * 2. originalType used for determining this ACK/NACK reply to Broadcast/Join message
 * @author router
 *
 */
public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int originalNodeNum;
	private String text = "";
	private ParcelType originalType = null;
	private UUID conversationId;
	
	Message () {}
	
	Message (int originalNodeNum, String text, ParcelType originalType) {
		this.setOriginalNodeNum(originalNodeNum);
		this.setText(text);
		this.setOriginalType(originalType);
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ParcelType getOriginalType() {
		return originalType;
	}

	public void setOriginalType(ParcelType originalType) {
		this.originalType = originalType;
	}

	public int getOriginalNodeNum() {
		return originalNodeNum;
	}

	public void setOriginalNodeNum(int originalNodeNum) {
		this.originalNodeNum = originalNodeNum;
	}
	
	public void setConversationId(UUID conversationId) {
		this.conversationId = conversationId;
	}
	
	public UUID getConversationId() {
		return this.conversationId;
	}
}
